#we test which method of masking the playing board is faster. The one with linear functions for each border
#or the one with image rotation
from homography_proof_of_concept import *
import time

if __name__ == "__main__":
    image, H = load_image_and_H("test3.jpg", "h3.pickle")
    image1 = copy.deepcopy(image)
    image2 = copy.deepcopy(image)
    image3 = copy.deepcopy(image)
    image4 = copy.deepcopy(image)
    
    points = transform_points(H)
    
    #playingboard points
    point1 = points[0]
    point2 = points[1]
    point3 = points[2]
    point4 = points[3]
    
    #grid points
    point5 = points[4]
    point6 = points[5]
    point7 = points[6]
    point8 = points[7]
    
    #robot points
    point9 = points[8]
    point10 = points[9]
    point11 = points[10]
    point12 = points[11]
    
    print("Standard implementation")
    #STANDARD
    start_time = time.process_time()
    get_mask_with_linear(image1, point1, point2, point3, point4)
    print(f"Extracting playingboard: {time.process_time() - start_time} seconds")
    
    start_time = time.process_time()
    get_mask_with_linear(image2, point5, point6, point7, point8)
    print(f"Extracting grid: {time.process_time() - start_time} seconds")

    
    start_time = time.process_time()
    get_mask_with_linear(image3, point9, point10, point11, point12, inverse=True)
    print(f"Covering robot: {time.process_time() - start_time} seconds")

    
    
    print("Faster implementation")
    #Faster implementation
    start_time = time.process_time()
    get_mask_with_linear_faster(image, point1, point2, point3, point4)
    print(f"Extracting playingboard: {time.process_time() - start_time} seconds")
    
    start_time = time.process_time()
    get_mask_with_linear_faster(image2, point5, point6, point7, point8)
    print(f"Extracting grid: {time.process_time() - start_time} seconds")
    
    start_time = time.process_time()
    get_mask_with_linear_faster(image3, point9, point10, point11, point12, inverse=True)
    print(f"Covering robot: {time.process_time() - start_time} seconds")
    