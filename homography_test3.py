import numpy as np
import cv2
import pickle

#okej, ideja
#do zdej sm gledu to kokr da morm celo sliko transformirat s homografijo potem pa naprej delat, ampak men se zdi da bi u resnici lahko
#delal na kirikol sliki potem pa sam na tocke izbrane bacil homografijo pa bi to mogl delat

def warpImage(img1, img2, H):
    '''warp img2 to img1 with homograph H'''
    h1,w1 = img1.shape[:2]
    #h2,w2 = img2.shape[:2]
    pts1 = float32([[0,0],[0,h1],[w1,h1],[w1,0]]).reshape(-1,1,2)
    #pts2 = float32([[0,0],[0,h2],[w2,h2],[w2,0]]).reshape(-1,1,2)
    pts2_ = cv2.perspectiveTransform(pts2, H)
    pts = concatenate((pts1, pts2_), axis=0)
    [xmin, ymin] = int32(pts.min(axis=0).ravel() - 0.5)
    [xmax, ymax] = int32(pts.max(axis=0).ravel() + 0.5)
    t = [-xmin,-ymin]
    Ht = array([[1,0,t[0]],[0,1,t[1]],[0,0,1]]) # translate

    result = cv2.warpPerspective(img2, Ht.dot(H), (xmax-xmin, ymax-ymin))
    return result

if __name__ == "__main__":
    myH = pickle.load(open("h.pickle", "rb"))
    image = cv2.imread("test.jpg")
    result = warpImage(