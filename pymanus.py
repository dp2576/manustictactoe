
import urllib, urllib.request
import json
import cv2

from math import cos, sin

import numpy as np

from dateutil.parser import parse as dateparser

def parse_timestamp(response):
    timestamp = response.info().get("X-Timestamp", "")
    if timestamp != "":
        return dateparser(timestamp)
    else:
        return None

class RemoteAPIError(Exception):
    def __init__(self, message):
        super().__init__(message)

class RemoteAPI(object):

    def __init__(self, server):
        self.server = server

    def request_json(self, path, arguments = {}):
        try:
            response = urllib.request.urlopen(self.server.generate(path, arguments))
            timestamp = parse_timestamp(response)

            return json.loads(response.read().decode(encoding = response.info().get_content_charset('utf-8'))), timestamp
        except urllib.error.URLError as e:
            raise RemoteAPIError("Remote API error: {}".format(e))

    def post_json(self, path, data, arguments = {}):
        try:
            request = urllib.request.Request(self.server.generate(path, arguments))
            request.add_header('Content-Type', 'application/json')
            response = urllib.request.urlopen(request, json.dumps(data).encode('utf-8'))
            timestamp = parse_timestamp(response)

            return json.loads(response.read().decode(encoding = response.info().get_content_charset('utf-8'))), timestamp
        except urllib.error.URLError as e:
            raise RemoteAPIError("Remote API error: {}".format(e))

class Server(object):

    def __init__(self, address, port, protocol = "http"):
        self.address = address
        self.port = port
        self.protocol = protocol

    def generate(self, path, arguments = {}):
        if arguments:
            return "{}://{}:{}/{}?{}".format(self.protocol, self.address, self.port, path, urllib.parse.urlencode(arguments))
        else:
            return "{}://{}:{}/{}".format(self.protocol, self.address, self.port, path)

class Manipulator(RemoteAPI):

    def __init__(self, server, name = "manipulator"):
        super().__init__(server)
        self.name = name

        data, _ = self.request_json("api/manipulator/describe")
        self.joints = data["joints"]

    def trajectory(self, goals, blocking = True):
        response, _ = self.post_json("api/manipulator/trajectory", goals, {"blocking" : blocking})
        return "result" in response and response["result"] == "ok"


    def move(self, goals, speed = 1.0, blocking = True):
        response, _ = self.post_json("api/manipulator/move", [{"goals" : goals, "speed" : speed}], {"blocking" : blocking})
        return "result" in response and response["result"] == "ok"

    def state(self):
        state, timestamp = self.request_json("api/manipulator/state")
        return state["joints"], timestamp

    def transformation(self, state):

        F = np.eye(4)

        for j, s in zip(self.joints, state):

            alpha = j["alpha"]
            theta = j["theta"]
            d = j["d"]
            a = j["a"]
            if j["type"] == "ROTATION":
                theta = s["position"]

            if j["type"] == "GRIPPER":
                break

            M = np.array([[cos(theta), -sin(theta) * cos(alpha), sin(theta) * sin(alpha), a * cos(theta)],
             [sin(theta), cos(theta) * cos(alpha), -cos(theta) * sin(alpha), a * sin(theta)],
             [0, sin(alpha), cos(alpha), d], [0, 0, 0, 1]])

            F = np.matmul(F, M)

        return F

    def position(self, state):

        F = self.transformation(state)

        P = np.matmul(F, np.array([[0], [0], [0], [1]]))

        return np.squeeze([P[0], P[1], P[2]])


class Camera(RemoteAPI):

    def __init__(self, server, name = "camera"):
        super().__init__(server)
        self.name = name

        data, _ = self.request_json("api/camera/describe")
        self.intrinsics = np.array(data["intrinsics"])
        self.distortion = np.array(data["distortion"])

    def position(self):
        data, timestamp = self.request_json("api/camera/position")

        rotation = np.array(data["rotation"])
        translation = np.array(data["translation"])

        return rotation, translation, timestamp

    def image(self):
        try:
            response = urllib.request.urlopen(self.server.generate("api/camera/image"))

            timestamp = parse_timestamp(response)

            image = np.asarray(bytearray(response.read()), dtype="uint8")
            return cv2.imdecode(image, cv2.IMREAD_COLOR), timestamp
        except urllib.error.URLError as e:
            raise RemoteAPIError("Remote API error: {}".format(e))
