import subprocess

class RobotNotFound(Exception):
    pass

def get_ip(robot_name = "roka4.local"):
    cmdCommand = f"ping {robot_name} -4" 
    process = subprocess.Popen(cmdCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    if 'Ping request could not find host' in output.decode('UTF-8'):
        raise  RobotNotFound
    ip = output.split()[2].decode('UTF-8').replace("[", "").replace("]", "")
    return ip
    

if __name__ == "__main__":
    get_ip()