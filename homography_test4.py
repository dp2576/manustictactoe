import cv2
import pickle
import numpy as np
#x,y = (-60, 250) kotex

if __name__ == "__main__":
    
    H = pickle.load(open("h.pickle", "rb"))
    image = cv2.imread("test.jpg")
    
    H2 = pickle.load(open("h2.pickle", "rb"))
    image2 = cv2.imread("test2.jpg")
    
    cv2.imshow("display", image)
    cv2.waitKey()
    
    # Center coordinates 
    x,y = (-60, 250) 
    
    p = np.array((x,y,1)).reshape((3,1))
    print(p)
    temp_p = H.dot(p)
    print(temp_p)
    suma = np.sum(temp_p ,1)
    print(suma)
    px = int(round(suma[0]/suma[2]))
    py = int(round(suma[1]/suma[2]))
    print(f"OG coordinates: {(x,y)}")
    print(f"Transformed: {(px, py)}")
    print("------------------------")
    center_coordinates = px, py
    # Radius of circle 
    radius = 5
       
    # Blue color in BGR 
    color = (255, 0, 0) 
       
    # Line thickness of 2 px 
    thickness = 5
       
    # Using cv2.circle() method 
    # Draw a circle with blue line borders of thickness of 2 px 
    image = cv2.circle(image, center_coordinates, radius, color, thickness)
    
    cv2.imshow("display", image)
    cv2.waitKey()   
    
    ##################################
    p = np.array((x,y,1)).reshape((3,1))
    temp_p = H2.dot(p)
    suma = np.sum(temp_p ,1)
    px = int(round(suma[0]/suma[2]))
    py = int(round(suma[1]/suma[2]))
    print(f"OG coordinates: {(x,y)}")
    print(f"Transformed: {(px, py)}")
    print("------------------------")
    center_coordinates = px, py
    # Radius of circle 
    radius = 5
       
    # Blue color in BGR 
    color = (255, 0, 0) 
       
    # Line thickness of 2 px 
    thickness = 5
       
    # Using cv2.circle() method 
    # Draw a circle with blue line borders of thickness of 2 px 
    image2 = cv2.circle(image2, center_coordinates, radius, color, thickness)
    
    cv2.imshow("display", image2)
    cv2.waitKey()   