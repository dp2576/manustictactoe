import cv2
import pickle
import numpy as np
from pymanus import Server, Manipulator, Camera
from setup import get_ip
import copy
import math

def load_image_and_H(name, nameH):
    image = cv2.imread(name)
    H = pickle.load(open(nameH, "rb"))
    return image, H
    
def apply_transform(H, point):
    x,y = point
    p = np.array((x,y,1)).reshape((3,1))
   
    temp_p = H.dot(p)
    suma = np.sum(temp_p ,1)
    
    px = int(round(suma[0]/suma[2]))
    py = int(round(suma[1]/suma[2]))
    return px, py

def transform_points(H):
    points_OG = [(-60, 250), (-60, -250), #upperleft, upperright
                (440, 250), (442, -255),  #bottomleft, bottomright
                (100, 205), (100, -200),  #upperleft grid, bottomright grid
                (375, 205), (375, -200),  #bottomleft grid, bottomright grid
                (-70, 80), (-70, -90),    #upperleft robot, upperright robot 
                (50, 80), (50, -90),      #bottomleft robot, bottomright robot
       
                ]  
    points = [apply_transform(H, p) for p in points_OG]
    return points

def get_homography(intrinsics, rotation, translation):
    #print(intrinsics, rotation, translation)

    transform = np.concatenate((rotation, np.transpose(translation)), axis=1)
    projective = np.matmul(intrinsics, transform)
    homography = projective[:, [0, 1, 3]]
    return homography / homography[2, 2]

def get_lin_function(point1, point2):
    x1 = point1[0]
    y1 = point1[1]
    x2 = point2[0]
    y2 = point2[1]
    
    n = y1 - x1*((y2-y1)/(x2 - x1))
    k = (y2-y1)/(x2- x1)
    
    return k, n

def get_mask_with_linear(image, point1, point2, point3, point4, inverse=False):
    
    #calculate the linear functions for each border
    k1,n1 = get_lin_function(point1, point2)
    k2,n2 = get_lin_function(point3, point4)
    k3,n3 = get_lin_function(point1, point3)
    k4,n4 = get_lin_function(point2, point4)
    
    if not inverse:
        #iterate over image and add the correct mask
        for i in range(len(image)):
            for j in range(len(image[i])):
                
                #upper border function
                if (i < (k1*j + n1)):
                    #print("A")
                    image[i][j] = np.array([0,0,0])
                    
                #bottom border function
                if(i > (k2*j + n2)):
                    image[i][j] = np.array([0,0,0])
                
                #left border function
                if(j < ((i-n3)/k3)):
                    image[i][j] = np.array([0,0,0])
                    
                
                #right border function
                if(j > ((i- n4)/k4)):
                    image[i][j] = np.array([0,0,0])
    else:
        #iterate over image and add the correct mask
        for i in range(len(image)):
            for j in range(len(image[i])):
                
                if (i > (k1*j + n1)) and (i < (k2*j + n2)) and (j > ((i-n3)/k3)) and (j < ((i- n4)/k4)):
                    #print("A")
                    image[i][j] = np.array([0,0,0])
                   
              
        
def get_mask_with_linear_faster(image, point1, point2, point3, point4, inverse=False):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    x4, y4 = point4
    
    h, w, _ = image.shape
    
    if y1 >= h:
        y1 = h - 1
    if y2 >= h:
        y2 = h - 1
    if y3 >= h:
        y3 = h - 1
    if y4 >= h:
        y4 = h - 1
    
    if x1 >= w:
        x1 = w - 1
    if x2 >= w:
        x2 = w - 1
    if x3 >= w:
        x3 = w - 1
    if x4 >= w:
        x4 = w - 1
        
    #we use < here because the coordinate system of the picture is inverse of the normal one (at least on the y axis)
    most_upper = y1 if y1 < y2 else y2
    
    most_bottom = y3 if y3 > y4 else y4
    
    most_right = x2 if x2 > x4 else x4
    
    most_left = x1 if x1 < x3 else x3
    
    #calculate the linear functions for each border
    k1,n1 = get_lin_function(point1, point2)
    k2,n2 = get_lin_function(point3, point4)
    k3,n3 = get_lin_function(point1, point3)
    k4,n4 = get_lin_function(point2, point4)
    
    if not inverse:
        image[:most_upper,:] = np.array([0,0,0])
        image[most_bottom:,:] = np.array([0,0,0])
        image[:,:most_left] = np.array([0,0,0])
        image[:,most_right:] = np.array([0,0,0])
        
        #iterate over image and add the correct mask
        for i in range(most_upper, most_bottom):
            for j in range(most_left, most_right):
                
                #upper border function
                if (i < (k1*j + n1)):
                    #print("A")
                    image[i][j] = np.array([0,0,0])
                    
                #bottom border function
                if(i > (k2*j + n2)):
                    image[i][j] = np.array([0,0,0])
                
                #left border function
                if(j < ((i-n3)/k3)):
                    image[i][j] = np.array([0,0,0])
                    
                
                #right border function
                if(j > ((i- n4)/k4)):
                    image[i][j] = np.array([0,0,0])
    else:
        #iterate over image and add the correct mask
        for i in range(most_upper, most_bottom):
            for j in range(most_left, most_right):
                
                if (i > (k1*j + n1)) and (i < (k2*j + n2)) and (j > ((i-n3)/k3)) and (j < ((i- n4)/k4)):
                    #print("A")
                    image[i][j] = np.array([0,0,0])
#okej, kaj hočem met
#sepravi da cel board zajame -> splosno
#potem da ceu grid zajame -> detekcija igralne povrsine in igranih figur
#robot bounding box -> za nastavljanje maske
#bounding boxi za detekcije robotovih figur
    
if __name__ == "__main__":
    image, H = load_image_and_H("test3.jpg", "h3.pickle")
    image2 = copy.deepcopy(image)
    image3 = copy.deepcopy(image)
    image4 = copy.deepcopy(image)
    
    points = transform_points(H)
    
    #playingboard points
    point1 = points[0]
    point2 = points[1]
    point3 = points[2]
    point4 = points[3]
    
    #grid points
    point5 = points[4]
    point6 = points[5]
    point7 = points[6]
    point8 = points[7]
    
    #robot points
    point9 = points[8]
    point10 = points[9]
    point11 = points[10]
    point12 = points[11]
        
    get_mask_with_linear_faster(image, point1, point2, point3, point4)
    cv2.imshow("display", image)
    cv2.waitKey()
    print("a")
    get_mask_with_linear_faster(image2, point5, point6, point7, point8)
    cv2.imshow("display", image2)
    cv2.waitKey()
    print("b")
    get_mask_with_linear_faster(image3, point9, point10, point11, point12, inverse=True)
    cv2.imshow("display", image3)
    cv2.waitKey()
    print("c")
    """cv2.imshow("Mask for playingboard", image)
    cv2.waitKey()
    
    get_mask_with_linear(image2, point5, point6, point7, point8)
    
    cv2.imshow("Mask for grid", image2)
    cv2.waitKey()
    
    get_mask_with_linear(image3, point9, point10, point11, point12, inverse=True)
    
    cv2.imshow("Mask for robot", image3)
    cv2.waitKey()
    
    k1,n1 = get_lin_function(point1, point2)
    
    pp1 = (0, round(k1*(0) + n1))
    pp2 = (900, round(k1*(900) + n1)) 
    
    cv2.line(image, pp1, pp2, (0,0,255), 2)
    
    ##################################
    point1 = points[2]
    point2 = points[3]
    
    k2,n2 = get_lin_function(point1, point2)
    
    pp1 = (0, round(k2*(0) + n2))
    pp2 = (900, round(k2*(900) + n2)) 
    
    cv2.line(image, pp1, pp2, (0,0,255), 2)
    
    ##################################
    point1 = points[0]
    point2 = points[2]
    
    k3,n3 = get_lin_function(point1, point2)
    
    pp1 = (0, round(k3*(0) + n3))
    pp2 = (900, round(k3*(900) + n3)) 
    
    cv2.line(image, pp1, pp2, (0,0,255), 2)
    
    
    ##################################
    point1 = points[1]
    point2 = points[3]
    
    k4,n4 = get_lin_function(point1, point2)
    
    pp1 = (0, round(k4*(0) + n4))
    pp2 = (900, round(k4*(900) + n4)) 
    
    cv2.line(image, pp1, pp2, (0,0,255), 2)
    
    
    
    for i in range(len(image)):
        for j in range(len(image[i])):
            
            #upper border function
            if (i < (k1*j + n1)):
                #print("A")
                image[i][j] = np.array([0,0,0])
                
            #bottom border function
            if(i > (k2*j + n2)):
                image[i][j] = np.array([0,0,0])
            
            #left border function
            if(j < ((i-n3)/k3)):
                image[i][j] = np.array([0,0,0])
                
            
            #right border function
            if(j > ((i- n4)/k4)):
                image[i][j] = np.array([0,0,0])
                
    print(image)
    for point in points:
        cv2.circle(image, point, 2, (255, 0,0), 3)
    
    cv2.circle(image, (0,60), 2, (0, 255, 0), 3)
    cv2.imshow("display", image)
    cv2.waitKey()
    #set up connection
    server = Server(get_ip("roka4.local"), 8080)
    manipulator = Manipulator(server)

    manipulator.state()

    camera = Camera(server)
    
    
    while True:
        intrinsics = camera.intrinsics
        rotation, translation, timestamp = camera.position()
        image, timestamp = camera.image()
        
        H = get_homography(intrinsics, rotation, translation)
        points = transform_points(H)
        
        point1 = points[0]
        point2 = points[1]
        
        k,n = get_lin_function(point1, point2)
        
        pp1 = (0, round(k*(0) + n))
        pp2 = (900, round(k*(900) + n)) 
        
        cv2.line(image, pp1, pp2, (0,0,255), 2)
        
        for x in image:
            print(x)
        for point in points:
            cv2.circle(image, point, 2, (255, 0,0), 3)
        
        cv2.circle(image, (0,60), 2, (0, 255, 0), 3)
        cv2.imshow("display", image)
        cv2.waitKey()
        
        get_board(image, [points[0], points[1], points[2], points[3]])
        break"""