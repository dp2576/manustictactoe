import pickle
import cv2
import numpy as np
import math
from pymanus import Server, Manipulator, Camera
import cv2
from setup import get_ip

def get_homography(intrinsics, rotation, translation):
    print(intrinsics, rotation, translation)

    transform = np.concatenate((rotation, np.transpose(translation)), axis=1)
    projective = np.matmul(intrinsics, transform)
    homography = projective[:, [0, 1, 3]]
    return homography / homography[2, 2]
    
def perspective_warp(image: np.ndarray, transform: np.ndarray):
    h, w = image.shape[:2]
    corners_bef = np.float32([[0, 0], [w, 0], [w, h], [0, h]]).reshape(-1, 1, 2)
    corners_aft = cv2.perspectiveTransform(corners_bef, transform)
    print(corners_aft)
    xmin = math.floor(corners_aft[:, 0, 0].min())
    ymin = math.floor(corners_aft[:, 0, 1].min())
    xmax = math.ceil(corners_aft[:, 0, 0].max())
    ymax = math.ceil(corners_aft[:, 0, 1].max())
    x_adj = math.floor(xmin - corners_aft[0, 0, 0])
    y_adj = math.floor(ymin - corners_aft[0, 0, 1])
    translate = np.eye(3)
    translate[0, 2] = -xmin
    translate[1, 2] = -ymin
    corrected_transform = np.matmul(translate, transform)
    return cv2.warpPerspective(image, corrected_transform, (math.ceil(xmax - xmin), math.ceil(ymax - ymin))), x_adj, y_adj

server = Server(get_ip("roka4.local"), 8080)
manipulator = Manipulator(server)

manipulator.state()

camera = Camera(server)

image, timestamp = camera.image()

intrinsics = camera.intrinsics
rotation, translation, timestamp = camera.position()


homography = get_homography(intrinsics, rotation, translation)

pieH = [[ -2.22670436e-01, -1.30580091e+00, 5.87428833e+02],
        [ 1.18497336e+00, -7.64347836e-02, 1.12789467e+02],
        [ -2.41243659e-04, -8.30100362e-06, 1.00000000e+00]]


"""myH = pickle.load(open("h.pickle", "rb"))
print(type(myH))
print("Raspie homography")
print("---------------------------")
for line in pieH:
    print(line)

print("My homography")
print("---------------------------")
for line in myH:
    print(line)


image = cv2.imread("test.jpg")"""

onetry, x, y = perspective_warp(image, homography)
cv2.imshow("overflow", onetry)
cv2.waitKey()
cv2.imwrite("stack2.jpg", onetry)
"""cv2.imshow("OG image", image)
cv2.waitKey()

pieHimage = cv2.warpPerspective(image, np.array(pieH), (3000, 3000))
myHimage = cv2.warpPerspective(image, myH, (3000, 3000))

cv2.imshow("pieH", pieHimage)
cv2.waitKey()

cv2.imshow("myH", myHimage)
cv2.waitKey()"""