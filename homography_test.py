from pymanus import Server, Manipulator, Camera
import cv2
from setup import get_ip
import numpy as np
import pickle

#server = Server('roka4.local', 8080)
#server = Server("192.168.1.135", 8080)

server = Server(get_ip("roka4.local"), 8080)
manipulator = Manipulator(server)

manipulator.state()

camera = Camera(server)

#get values for homography calculation
intrinsics = camera.intrinsics
rotation, translation, timestamp = camera.position()


image, timestamp = camera.image()
print(image.shape)

cv2.imshow("display", image)
cv2.waitKey()
cv2.imwrite("test3.jpg", image)
#rotation
#translation
#intrinsics
def get_homography(intrinsics, rotation, translation):
    print(intrinsics, rotation, translation)

    transform = np.concatenate((rotation, np.transpose(translation)), axis=1)
    projective = np.matmul(intrinsics, transform)
    homography = projective[:, [0, 1, 3]]
    return homography / homography[2, 2]
    

homography = get_homography(intrinsics, rotation, translation)
file = open("h3.pickle", "wb")
pickle.dump(homography, file)
file.close()
print(homography)

a = np.array([[1, 0, 0],
              [0, 1, 0],
              [0, 0, 1]],
             dtype=np.float32)
             
#center = np.dot(homography, np.transpose(np.array([1280/2, 720/2, 1], dtype='float32')))


blank_image = np.zeros((720, 1280,3), np.uint8)
warped = cv2.warpPerspective(image, homography, (720, 1280))#, flags=cv2.WARP_INVERSE_MAP)

cv2.imshow("Display", warped)
cv2.waitKey()