from pymanus import Server, Manipulator, Camera
import cv2
from setup import get_ip


#server = Server('roka4.local', 8080)
#server = Server("192.168.1.135", 8080)

server = Server(get_ip("roka4.local"), 8080)
manipulator = Manipulator(server)

manipulator.state()

camera = Camera(server)

image, timestamp = camera.image()
cv2.imshow("display", image)
cv2.waitKey()
