
import random, sys

import numpy as np

from functools import partial

import datetime

from pymanus import Server, Manipulator, Camera
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scipy.spatial.distance import cdist

from pointmatch import fit_model, draw_origin, join_homogenous

import cv2

import os.path

import pickle

BATCH_SIZE = 500

WORKSPACE_BOUNDS = [[100, 300], [-150, 150], [60, 300]]

#apt-get install v4l-utils
# v4l2-ctl -c focus_auto=0,focus_absolute=0

#CALIBRATION_POINTS = np.array([[0, 40, 0], [0, 80, 0], [0, -40, 0], [0, -80, 0], [40, 0, 0], [80, 0, 0], [60, 60, 0], [60, -60, 0], [4, 0, -60], [4, 0, 60]])
#CALIBRATION_POINTS = np.array([[-40, 40, 0], [-40, 80, 0], [-40, -40, 0], [-40, -80, 0], [0, 0, 0], [40, 0, 0], [20, 60, 0], [20, -60, 0], [-36, 0, -60], [-36, 0, 60]])
#CALIBRATION_POINTS = np.array([[-40, 40, 0], [-40, 80, 0], [-40, -40, 0], [-40, -80, 0], [0, 0, 0], [40, 0, 0], [20, 60, 0], [20, -60, 0]])
CALIBRATION_POINTS = np.array([[0, 40, -40], [0, 80, -40], [0, -40, -40], [0, -80, -40], [0, 0, 0], [0, 0, 40], [0, 60, 20], [0, -60, 20]])
#CALIBRATION_POINTS = [[0, 0, 0], [0, 0, 100], [100, 0, 0]]

def generate_position(current = None):
    if current is None:
        return [random.randint(WORKSPACE_BOUNDS[0][0], WORKSPACE_BOUNDS[0][1]), \
                random.randint(WORKSPACE_BOUNDS[1][0], WORKSPACE_BOUNDS[1][1]), \
                random.randint(WORKSPACE_BOUNDS[2][0], WORKSPACE_BOUNDS[2][1])]
    else:
        current = [int(x) for x in current]
        return [random.randint(max(WORKSPACE_BOUNDS[0][0], current[0] - 50), min(WORKSPACE_BOUNDS[0][1], current[0] + 50)), \
                random.randint(max(WORKSPACE_BOUNDS[1][0], current[1] - 50), min(WORKSPACE_BOUNDS[1][1], current[1] + 50)), \
                random.randint(max(WORKSPACE_BOUNDS[2][0], current[2] - 50), min(WORKSPACE_BOUNDS[2][1], current[2] + 50))]

SERVER = sys.argv[1]

save = True

save_file = 'calibration_data_{}.pkl'.format(SERVER)

server = Server('{}.local'.format(SERVER), 8080)

manipulator = Manipulator(server)

camera = Camera(server)

if save:
    if os.path.isfile(save_file):
        batch = pickle.load(open(save_file, "rb"))
    else:
        batch = {"states": np.empty([0, len(manipulator.joints)]), "positions" : np.empty([0, 3]), "camera" : np.empty([0, 6]), "measured" : np.empty([0, 6]), "joints" : manipulator.joints}

    samples = batch["states"].shape[0]
else:
    samples = 0

detector = cv2.SimpleBlobDetector_create()

flip = np.eye(4)
flip[2, 2] = -1

real_position = None
tries = 0

while samples < BATCH_SIZE:

    tries = tries + 1

    if tries % 20 == 0:
        real_position = None

    position = generate_position(real_position)

    if manipulator.trajectory([{"location" : position, "grip" : 0, "speed" : 1}]):

        state, _ = manipulator.state()
        real_position = manipulator.position(state)

        # Hack: randomize last joint because it is sometimes put into extreme position
        if tries % 5 == 0:
            goals = [p["position"] for p in state]
            goals[-2] = random.uniform(manipulator.joints[-2]["min"], manipulator.joints[-2]["max"])
            manipulator.move(goals)

        cv2.waitKey(1000)

        rotation, translation, positiontime = camera.position()
        state, statetime = manipulator.state()

        while True:

            image, imagetime = camera.image()

            if imagetime > positiontime and imagetime > statetime:
                break

            cv2.waitKey(1000)


        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        keypoints = detector.detect(255 - hsv[:, :, 1])

        imagepoints = np.empty([0, 2])

        for kp in keypoints:
            hue = hsv[int(kp.pt[1]), int(kp.pt[0]), 0]
            value = hsv[int(kp.pt[1]), int(kp.pt[0]), 2]
            sat = hsv[int(kp.pt[1]), int(kp.pt[0]), 1]
            if (hue < 20 or hue > 240) and value > 60 and sat > 100:
                cv2.circle(image, (int(kp.pt[0]), int(kp.pt[1])), 4, (255, 200, 10), 2)
                imagepoints = np.append(imagepoints, np.reshape([int(kp.pt[0]), int(kp.pt[1])], (1, 2)), axis=0)

        if imagepoints.shape[0] < 5:
            continue

        F = np.matmul(flip, manipulator.transformation(state))

        rotation, _ = cv2.Rodrigues(rotation)

        dr, dt, q, error = fit_model(imagepoints, CALIBRATION_POINTS, F, rotation, translation, camera.intrinsics, camera.distortion)

        if error > 10:
            continue

        draw_origin(image, np.eye(4), dr, dt, camera.intrinsics, camera.distortion)
        draw_origin(image, F, rotation, translation, camera.intrinsics, camera.distortion)

        camera_transform = np.matmul(np.linalg.inv(join_homogenous(rotation, translation)), join_homogenous(dr, dt))

        camera_position = np.matmul(camera_transform, [[0], [0], [0], [1]])
        manipulator_position = np.matmul(F, [[0], [0], [0], [1]])

        print(camera_position)
        print(manipulator_position)

        error = np.linalg.norm(camera_position - manipulator_position)

        samples = samples + 1

        print("Sample {}: error {}".format(samples, error))

        if save:
            batch["states"] = np.append(batch["states"], [[j["position"] for j in state]], axis=0)
            batch["positions"] = np.append(batch["positions"], [real_position], axis=0)
            batch["camera"] = np.append(batch["camera"], [np.append(rotation, translation)] , axis=0)
            batch["measured"] = np.append(batch["measured"], [np.append(dr, dt)] , axis=0)
            if samples % 10 == 0:
                pickle.dump(batch, open(save_file, "wb"))

        cv2.imshow("Camera: {}".format(SERVER), image)
        cv2.waitKey(10)


#plt.show()


